package com.company;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Gson gson = new Gson();
        Scanner scan = new Scanner(System.in);
        boolean exit = false;

        try {
            System.out.printf("Enter the name of json file which you want to load the grammar of: ");
            String input = scan.nextLine();

            //Open some JSON file and deserialize it
            BufferedReader br = new BufferedReader(new FileReader(input));
            LameCFG CFG = gson.fromJson(br, LameCFG.class);

            while(!exit) {
                System.out.print("Enter a string to check if it exists in the language using the alphabet (");
                for (Character s : CFG.alphabet) { //Print out alphabet
                    System.out.print(s + ",");
                }
                System.out.print("): ");
                input = scan.nextLine();
                String[] separatedInput = input.split("");

                //If returns true then print out message
                if (CFG.checkString(input)) {
                    System.out.println(input + " is contained in the language");
                } else {
                    System.out.println(input + " is not contained in the language");
                }

                System.out.print("Do you want to exit? ");
                input = scan.nextLine();

                if(input.toLowerCase().equals("yes") || input.toLowerCase().equals("y")) {
                    exit = true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
