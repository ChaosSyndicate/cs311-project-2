package com.company;

import java.util.Map;
import java.util.Set;

public class LameCFG {
    public Map<String, Map<Character, String>> rules;
    public Set<String> terminals;
    public Set<Character> alphabet;

    public boolean checkString(String word) {
        String stack = "S";
        String currTerminal = "S";
        String rule;

        try {
            for (int i = 0; i < word.length(); i++) {
                for (int j = 0; j < stack.length(); j++) {
                    Character c = stack.charAt(j);
                    if (terminals.contains(c.toString())) {
                        currTerminal = c.toString();
                        rule = rules.get(currTerminal).get(word.charAt(i));
                        stack = stack.replaceFirst(currTerminal, rule);
                        break;
                    }
                }
            }
            return (stack.equals(word));
        } catch (Exception e) {
            return false;
        }
    }
}
